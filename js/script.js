$(document).ready(function () {

	$("#spotlight header .mobile").click(function () {
		$("#spotlight header .mobile").hide();
		$("#spotlight header .close").show();
		$("#spotlight .menu-container").slideDown("slow", function () {});
	});

	$("#spotlight header .close").click(function () {
		$("#spotlight header .close").hide();
		$("#spotlight header .mobile").show();
		$("#spotlight .menu-container").slideUp("slow", function () {});
	});

    $("#participate .container .button").click(function () {
		$(".modal-wrapper").show();
	});
    $("#form .head span").click(function () {
		$(".modal-wrapper").hide();
	});

    $('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;
  
    $this.addClass('select-hidden'); 
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());
  
    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);
  
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }
  
    var $listItems = $list.children('li');
  
    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });
  
    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });
  
    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});

var input = document.getElementById( 'file-upload' );
var infoArea = document.getElementById( 'file-upload-filename' );

input.addEventListener( 'change', showFileName );

function showFileName( event ) {
  
  // the change event gives us the input it occurred in 
  var input = event.srcElement;
  
  // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
  var fileName = input.files[0].name;
  
  // use fileName however fits your app best, i.e. add it into a div
  infoArea.textContent = 'File name: ' + fileName;
}

var wow = new WOW({
    boxClass: "wow", // animated element css class (default is wow)
    animateClass: "animated", // animation css class (default is animated)
    offset: 0, // distance to the element when triggering the animation (default is 0)
    mobile: true, // trigger animations on mobile devices (default is true)
    live: true, // act on asynchronously loaded content (default is true)
    callback: function (box) {
    },
		scrollContainer: null, // optional scroll container selector, otherwise use window,
		resetAnimation: true, // reset animation on end (default is true)
  	});
  wow.init();
});

$(window).scroll(function () {
  var scroll_pos = 0;


  scroll_pos = $(this).scrollTop();

  if (scroll_pos > 0) {
    $("#spotlight header").css("background-color", "#fef9f6");
    $("#spotlight header").css("box-shadow", "rgb(68 68 68 / 5%) 2px 3px 3px");
  } else if (scroll_pos == 0) {
    $("#spotlight header").removeAttr("style");
  }
  
});

// hamburger-menu
function showMenu() {
  $("body").toggleClass("active")
}

function hideMenu() {
  $("body").removeClass("active")
}
